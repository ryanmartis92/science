---
title: 12 Earth and Space Science
menu: "main"
linkTitle: "12ESS"
---

## Units

1. [Meteorite Investigation](1-investigation/)
2. [Extreme Earth Events](2-extreme-earth-events/)


## 2020 Year Planner

|          | Term 1      |                          | Term 2    |                                    | Term 3  |                                    | Term 4   |             |
|:---------|:------------|:-------------------------|:----------|:-----------------------------------|:--------|:-----------------------------------|:---------|:------------|
| Week 1   | Jan 27th    | Meteorite Investigation  | Apr 27th  | Extreme Earth Events               | Jul 20  | Life of Stars & Planetary Systems  | Oct 12   | Revision    |
| Week 2   | Feb 3rd     | Meteorite Investigation  | May 4th   | Extreme Environments               | Jul 27  | Life of Stars & Planetary Systems  | Oct 19   | Revision    |
| Week 3   | Feb 10th    | Meteorite Investigation  | May 11    | Extreme Environments               | Aug 3   | Life of Stars & Planetary Systems  | Oct 26   | Revision    |
| Week 4   | Feb 17th    | Meteorite Investigation  | May 18    | Extreme Environments               | Aug 10  | Senior Exams                       | Nov 2    | Revision    |
| Week 5   | Feb 24th    | Meteorite Investigation  | May 25    | Extreme Environments               | Aug 17  | Senior Exams                       | Nov 9    | Exam Leave  |
| Week 6   | March 2nd   | Extreme Earth Events     | Jun 1     | Extreme Environments               | Aug 24  | Geological Processes               | Nov 16   | Exam Leave  |
| Week 7   | March 9th   | Extreme Earth Events     | Jun 8     | Life of Stars & Planetary Systems  | Aug 31  | Geological Processes               | Nov 23   | Exam Leave  |
| Week 8   | March 16th  | Extreme Earth Events     | Jun 15    | Life of Stars & Planetary Systems  | Sep 7   | Geological Processes               | Nov 30   | Exam Leave  |
| Week 9   | March 23rd  | Extreme Earth Events     | Jun 22    | Life of Stars & Planetary Systems  | Sep 14  | Geological Processes               | Dec 7th  | Exam Leave  |
| Week 10  | March 30th  | Extreme Earth Events     | Jun 29    | Life of Stars & Planetary Systems  | Sep 21  | Geological Processes               |          |             |
| Week 11  | April 6th   | Extreme Earth Events     |           |                                    |         |                                    |          |             |
