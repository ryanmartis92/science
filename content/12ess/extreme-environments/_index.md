---
title: AS91190 - Extreme Environments
menu:
    main:
        parent: "12ESS"
---

> Investigate how organisms survive in an extreme environments.

__NZQA Link:__ [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91190](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91190)

{{< slides >}}

## Unit Plan


|        | Lesson 1              | Lesson 2                 | Lesson 3                  | Lesson 4          |
|--------|-----------------------|--------------------------|---------------------------|-------------------|
| Week 1 |                       | Introduction             | MRS C GREN                | Education Perfect |
| Week 2 | Intro to Adaptations  | Structural & Behavioural | Physiological             | Law of Tolerance  |
| Week 3 | Humans in Antartctica |                          |                           |                   |
| Week 4 | Case Study            | Case Study               | Start Assessment Research |                   |

## Lesson Plans

1. Introduction
    - Read through the slides attached
    - Write down any definitions you encounter
    - Complete the environment identification task inside the slides
    - Complete the definition mix-and-match inside the slides
2. MRS C GREN
    - Follow the slides
    - Complete the table inside the slides
    - Complete the research task about an animal
3. Education Perfect
    - Do the Year 9 & 10 recap tasks on Education Perfect
4. Introduction to Adaptations
    - Slides and tasks in slides
    - Quizlet best of three competition
    - Evolve a Human 2.0
5. Structural & Behavioural Adaptations
    - Adaptations of the human hand
    - Watch video and make notes on adaptations
6. Physiological Adaptations
7. Law of Tolerance
    - Egg denaturing practical https://www.riskassess.co.nz/risk_assessment/8044485
8. Antarctica
    - Diving in Antarctica video (slide 24)
    - Notes on 25-34
    - Clothing task on slides 35-36
    - Finish mapping task from earlier in the week