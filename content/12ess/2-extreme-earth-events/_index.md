---
title: AS91191 - Extreme Earth Events
menu:
    main:
        parent: "12ESS"
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91191](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91191)

{{< slides >}}

## Unit Plan

|             | Monday                                 | Wednesday                 | Thursday                  | Friday                            |
|:------------|:---------------------------------------|:--------------------------|:--------------------------|:----------------------------------|
| T1 Week 6   | Earth's Structure                      | Earth's Structure         | Plate Tectonics           | Plate Tectonics                   |
| T1 Week 7   | NZ Plate Tectonics                     | Volcanoes                 | Volcanoes                 | Magma Types                       |
| T1 Week 8   | Types of Volcanoes                     | Types of Volcanoes        | Volcanic Hazards          | Volcanic Hazards                  |
| T1 Week 9   | Taupo Volcanic Zone                    | Auckland Volcanic Zone    | Earthquakes               | Locating & Measuring Earthquakes  |
| T1 Week 10  | Effect of Earthquakes                  | Alpine Fault              | Tsunamis                  | Tsunamis                          |
| T1 Week 11  | Underwater Avalanches \n & Landslides  | Answering Exam Questions  | Answering Exam Questions  | Answering Exam Questions          |
| T2 Week 1   | Revision                               | Revision                  | Revision                  | __End of Topic Test__             |

## Lesson Plans

## Term 2, Week 2

__Google Meets:__ Wednesday 9:35am

### Lesson 1

- Read through & make notes from Introduction to Earthquakes slides
- Use Textbook Page 167 to help with the diagram annotation (attached)

### Lesson 2

- Google Meet Wednesday 9:35am
- Earthquakes Waves Document
- Earthquakes Education Perfect Task

### Lesson 3

- Read NZ Earthquakes slides
- Complete task on Alpine Fault 

### Lesson 4

- Complete the earthquakes recap document
- Complete pages 178-179 in textbook (attached)
- Complete Education Perfect assessment (practice)

---

## Term 2, Week 3

This week is a short week so there'll only be three lessons. Two on tsunamis and one revision. After this we are done with our learning and will be having an online assessment next Tuesday evening!

__Google Meet:__ Wednesday 9:35am

### Lesson 1

- Copy the tsunami diagram from the notes into your book
- Complete the Ted ED activity
- Do the Quizziz until you get 100%. When you do, upload a screenshot to this assignment!

### Lesson 2

- Complete the ordering task
- Complete the Education Perfect task
- Do textbook pages 180-187
- Upload photos/screenshots of your ordering task and textbook work once you are done

### Lesson 3

You have three options of what to work on for revision, they are given below in order of most to least useful. I HIGHLY recommend working on the past exam questions before anything else.

1. Past Exam Questions - Scaffolded
2. Tsunami Diagramming Practice
3. Quizlet Set

Remember to upload screenshots/photos of any work that you complete!