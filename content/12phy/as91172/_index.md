---
title: Atomic and Nuclear Physics
menu:
    main:
        parent: "12PHY"
---

__NZQA__: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91172](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91172)

{{< slides >}}

## Unit Plan

|        | Lesson 1 (Mon) | Lesson 2 (Tues) | Lesson 3 (Wed) | Lesson 4 (Thurs) |
|--------|----------------|-----------------|----------------|------------------|
| T3 W10 | 1.             | 2.              | 3.             | 4.               |
| T4 W1  | 5.             | 6.              | 7.             | 8.               |
| T4 W2  | 9.             | 10.             | 11.            | 12. __Test__     |

### Lesson Plans

1. Atoms & Isotopes
2. Discovering the Atomic Structure
    - Homework Booklet: 2004 Q2, 2005 Q1-2, 2006 Q1, 2007 Q1
    - Worksheet 1: Questions 1-4
    - Worksheet 3: Questions 1-2
3. Radioactive Decay
    - Worksheet 1: Questions 5-10
    - Worksheet 2: Questions 1-3, 4a, 4b, 5a, 5b, 6a, 6b
4. Nuclear Equations
    - Homework Booklet: 2004 Q1, 2004 Q4, 2005 Q5
    - Worksheet 2: Questions: 4c, 5c, 6c 7-9
5. Half-Life
    - Homework Booklet: 2004 Q3, 2005 Q7, 2006 Q4, 2005 Q9
    - Worksheet 3: Questions: 3-6
6. Fission & Fusion
