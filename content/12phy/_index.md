---
title: 12 Physics
menu: "main"
linkTitle: "12PHY"
---

1. [AS91171 - Mechanics](as91171/)
    - External, 6 Level 2 Credits
2. [AS91173 - Electricity](as91173/)
    - External, 6 Level 2 Credits
3. [AS91523 - Wave Systems](as91523/)
    - External, 4 Level 3 Credits
4. [AS91172 - Atomic and Nuclear Physics](as91172/)
    - Internal, 3 Level 2 Credits

## 2020 Year Planner

|         | Term 1         |           | Term 2         |              | Term 3     |                  | Term 4      |                 |
|---------|----------------|-----------|----------------|--------------|------------|------------------|-------------|-----------------|
| Week 1  | __Jan 27th__   | Mechanics | __Apr 13th__   | Mechanics    | __Jul 20__ | Wave Systems     | __Oct 12__  | Nuclear Physics |
| Week 2  | __Feb 3rd__    | Mechanics | __April 20th__ | Electricity  | __Jul 27__ | Wave Systems     | __Oct 19__  | Nuclear Physics |
| Week 3  | __Feb 10th__   | Mechanics | __April 27th__ | Electricity  | __Aug 3__  | _Revision_       | __Oct 26__  | _Revision_      |
| Week 4  | __Feb 17th__   | Mechanics | __May 4th__    | Electricity  | __Aug 10__ | __School Exams__ | __Nov 2__   | __NCEA Exams__  |
| Week 5  | __Feb 24th__   | Mechanics | __May 11th__   | Electricity  | __Aug 17__ | __School Exams__ | __Nov 9__   | __NCEA Exams__  |
| Week 6  | __March 2nd__  | Mechanics | __May 18th__   | Electricity  | __Aug 24__ | Wave Systems     | __Nov 16__  | __NCEA Exams__  |
| Week 7  | __March 9th__  | Mechanics | __May 25th__   | Electricity  | __Aug 31__ | Wave Systems     | __Nov 23__  | __NCEA Exams__  |
| Week 8  | __March 16th__ | Mechanics | __June 1st__   | Electricity  | __Sep 7__  | Wave Systems     | __Nov 30__  | __NCEA Exams__  |
| Week 9  | __March 23rd__ | Mechanics | __June 8th__   | Electricity  | __Sep 14__ | Wave Systems     | __Dec 7th__ | __NCEA Exams__  |
| Week 10 |                |           | __June 15th__  | Electricity  | __Sep 21__ | Nuclear Physics  |             |                 |
| Week 11 |                |           | __June 22nd__  | Electricity  |            |                  |             |                 |
| Week 12 |                |           | __June 29th__  | Wave Systems |            |                  |             |                 |

 