---
title: Magnetic Fields
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
weight: 8
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

The battery is 12V.

![](../assets/8-circuit.jpg){width=30%}

1. When switched on, how many joules of energy does the cell supply to each coulomb of charge that flows out of the cell? __(A)__

---

![](../assets/8-circuit.jpg){width=30%}

2. When the cell is switched on, the resistance of the lamp is $9\Omega$. Calculate the current flowing through the lamp. __(A)__
3. State the meaning of the term __resistance__ in terms of electron flow. __(A)__

---

# Magnetic Fields

Magnetic fields can be found in two places:

- Around magnetic objects
- Around current carrying wires

---

![](../assets/8-bar-magnet.jpg)

---

![](../assets/8-iron-filings.jpg)

---

![](../assets/8-attraction-repulsion.png)

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/aVqN1tW1k7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

![](../assets/8-earth-magnetic-field.jpg){width=75%}

---

## Drawing Magnetic Fields on Paper

![](../assets/8-field-in-and-out.png)

---

## Current Carrying Wire

![](../assets/8-field-wire.jpg){width=50%}

---

## Right Hand Grip Rule 1

- Used to find the direction of the current or magnetic field when you know the other.
- This version of the right hand grip rule only applies to current carrying wires!

![](../assets/8-right-hand-rule-1.gif){width=40%}

---

![](../assets/magnetic_fields-current-carrying-wire.png)

---

## Right Hand Grip Rule 2

- Used to find the direction of the magnetic field in a coil of wire.

![](../assets/8-right-hand-grip-2.png){width=50%}

---

## Magnetic Elements

![](../assets/8-magnetic-p-table.jpg){width=75%}

---

### Ferromagnetism

- Includes Co, Fe and Ni
- Permanent magnets
- Were exposed to a strong external magnetic field and retained the field after being removed from it

---

### Paramagnetism

- Includes Na, Mg, Ca etc.
- Can form a weak internal magnetic field when exposed to an external magnetic field
- Feel a small attractive force

---

### Diamagnetism

- Includes Cu, Si, N, C etc.
- Can form a weak internal magnetic field when exposed to an external magnetic field
- Feel a small repulsive force

![](../assets/magnetism-diamagnetism.jpg){width=50%}

---

![](../assets/8-q1.png)

---

![](../assets/8-q2.png)

---

![](../assets/8-q3.png)
