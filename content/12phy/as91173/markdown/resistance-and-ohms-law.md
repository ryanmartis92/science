---
title: Resistance & Ohm's Law
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
weight: 4
theme: finn
colortheme: dolphin
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

Give the definition, symbol, unit and formula for the following:

1. Electric Potential Energy
2. Current
3. Voltage
4. Power

---

Recall that a circuit is an energy delivery system!

## Curent
\begin{align*}
    & current = \frac{charge}{time} \\\\
    & I = \frac{q}{t}
\end{align*}

---

## Voltage
\begin{align*}
    & voltage = \frac{\text{Electric field strength}}{charge} \\\\
    & V = \frac{E}{q}
\end{align*}

---

## Power

\begin{align*}
    & power = current \times voltage \\\\
    & P = IV
\end{align*}

What are we missing?

---

# Resistance

- __Q:__ What carries the charge in a circuit?
- __A:__ Typically, electrons ($e^{-}$)

---

- __Q:__ Why are they able to flow in metals?
- __A:__ Because electrons exist in a "sea", not bound to one atom but able to move around the solid

---

- __Q:__ What is resistance?
- __Resistance__ is the measure of how much electrons are impeded in a circuit. How much they are _slowed down_.

---

## Symbol & Units

__Resistance__ has symbol __R__ in equations and has the unit __Ohms__ ($\Omega$, the Greek letter omega).

---

## Resistance & Heat

When current moves through a material with resistance the electrons _bump_ into the nuclei. This causes energy to be transferred in the form of vibrations (__heat__)!

The higher the resistance, the more heat produced!

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/Y-LPERlRHYA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Ohm's Law

\begin{align*}
    & V = IR \\\\
    & voltage = current \times resistance \\\\
\end{align*}

- Voltage is measured in..
- Current is measured in..
- Resistance is measured in..

---

### Examples

1. The resistance of a light bulb is $1.5k\Omega$. Calculate the current through the bulb when it is connected across a $12V$ power supply.
2. When $9V$ is applied to a resistor, $0.03mA$ of current flows through it. Calculate the resistance of the resistor.
3. How much voltage is required to produce $180\mu A$ of current flowing through a $0.6M\Omega$ resistor?

---

### Question 1 Answer

\begin{align*}
    & V = IR \\\\
    & I = \frac{V}{R} \\\\
    & I = \frac{12}{1500} \\\\
    & I = 0.008A \\\\
\end{align*}

---

### Question 2 Answer

\begin{align*}
    & V = IR \\\\
    & R = \frac{V}{I} \\\\
    & R = \frac{9}{0.00003} \\\\
    & R = 300000\Omega \\\\
\end{align*}

---

#### Question 3 Answer

\begin{align*}
    & V = IR \\\\
    & V = (180 \times 10^{-6}) \times (0.6 \times 10^{6}) \\\\
    & V = 108V \\\\
\end{align*}
