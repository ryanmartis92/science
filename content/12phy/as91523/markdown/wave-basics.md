---
title: Wave Basics
subtitle: 12PHYS - Wave Systems
author: Finn LeSueur
date: 2020
weight: 1
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

## About the Unit

- Wave Systems
- AS91523
- 4 Credits, Level 3 (External)
- Not in the school exams!
- End of topic test in Term 3 Week 9

---

To start this unit off we are going to spend a week doing some learning about the fundamentals of waves from the Year 12 paper, before moving on to Year 13 specific stuff.

---

## What is a wave?

Energy can be transferred from one place to another through either kinetic or electromagnetic waves.

---

### Mechanical Waves

Waves that are vibrations pass through a solid or other medium. Think, the ocean or a slinky.

__NB:__ They __require__ a medium in which to travel.

---

### Electromagnetic Waves

Waves that are part of the electromagnetic spectrum (light, radio waves, infrared, etc).

__NB:__ They __do not require__ a medium in which to travel. Can travel through the vaccuum.

---

The waves that we will be looking at this year have repetition in their motion, and are called __periodic waves__. This periodicity allows us to predict their interactions with relative ease.

---

## Transverse vs Longitudinal Waves

- __Longitudinal:__ The vibration (displacement) is parallel to the direction of motion of the wave (e.g. sound). Think of compression and expansion.
- __Transverse:__ The vibration (displacement) is perpendicular to the direction of motion of the wave (e.g. electromagnetic waves like light).

---

![](https://media.giphy.com/media/og52So0BUmZVe/source.gif "")

---

![](https://media.giphy.com/media/G7Pc0fNwuVzYk/source.gif "")

---

## Labelling a Wave

__Task:__ Sketch this unlabelled wave into your book on the axis.

![](../assets/transverse-longitudinal.jpg "")

---

- __Peak:__ The highest point on a wave
- __Trough:__ The lowest point on a wave
- __Amplitude (A):__ The point of greatest displacement from rest on the wave (midpoint to peak, midpoint to trough) (in $m$)
- __Wavelength $\lambda$:__ The distance (in $m$) between consecutive points of idential displacement on the wave (peak to peak, trough to trough, midpoint to midpoint etc.)

---

- __Compression:__ The point where the wavelength is most squished together
- __Expansion:__ The point where the wavelength is most spread out
- __Velocity $v$:__ The velocity (in $ms^{-1}$) of the wave shape

---

![](https://media.giphy.com/media/og52So0BUmZVe/source.gif "")

---

![](../assets/transverse-longitudinal-labelled.jpg "")

---

### Period vs. Frequency

- __Period $T$:__ The time taken, in $s$, for one wave to pass any point. For example, a period of 5s means it takes 5 seconds for consecutive peaks to pass a given point.
- __Frequency $f$:__ The number of waves that pass a point in a second, measured in "per second" $s^{-1}$, better known as Hertz $Hz$.

![](https://media.giphy.com/media/og52So0BUmZVe/source.gif "")

---

The period is very closely linked to the frequency as you can see from their definitions. There is a very simple formula for converting between them:

\begin{align*}
    & T = \frac{1}{f} \\\\
    & f = \frac{1}{T}
\end{align*}

---

We can also relate the __velocity__, __frequency__, and __wavelength__ with the __wave equation__:

$$
v = f\lambda
$$

---

![](../assets/transverse-wave.gif "")

---

![](../assets/longitudinal-wave.gif "")

---

### Interference

When waves intersect each other, they __interfere__. This is where the amplitudes of the waves are combined (add positive and negative amplitudes).

- __Constructive Interference__: When two peaks meet and the resulting amplitude is greater
- __Destructive Interference__: When a peak and a trough meet and the resulting amplitude is less (or in some cases, zero)

---

![](../assets/interference.gif "")

---

### Nodes and Anti-Nodes

- A node (think: no amplitude) is a place of minimum amplitude
- An anti-node is a place of maximum amplitude

![](../assets/nodes-antinodes.png "")

---

## Starter

Mathieu is surfing at Taylors Mistake and decides to count the waves. He notices that 3 waves pass him 60s.

1. What is the frequency of the waves?
2. What is the period of the waves?
3. A lifeguard measures the velocity of the waves to $300cms^{-1}$. What is their wavelength?

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/IXxZRZxafEQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Calculation

Red light has a wavelength of approximately $400nm$ and travels at approximately $3x10^{8}ms^{-1}$.

1. Calculate the frequency of this wavelength light.
2. Explain, in words, what frequency means in this context.

---

## Diffraction

Diffraction is when waves change direction as they encounter an obstacle or pass through an aperture.

- Long wavelengths diffract more (bend more) around obstacles
- Wavelengths closer to the diameter of an aperture diffract more than those not similar

---

![](http://hyperphysics.phy-astr.gsu.edu/hbase/Sound/imgsou/difr2.png "")

---
 