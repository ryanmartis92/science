---
title: Wave Systems - AS91523
menu:
    main:
        parent: "12PHY"
katex: true
---

__NZQA:__ [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91523](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=91523)

{{< slides >}}

## Learning Outcomes

1. 

## Homework

1. Term 3, Week 1 (Fri July 25th): Wave Fundamentals Question 1 & 2
2. Term 3, Week 2 (Mon July 27th): Doppler Question 9
3. Term 3, Week 3 (Mon July 3rd): 
4. Term 3, Week 6 (Mon August 24th): 
5. Term 3, Week 7 (Mon August 31st): 
6. Term 3, Week 8 (Mon Sept 7th): 
7. Term 3, Week 9 (Mon Sept 14th):  

## Unit Plan


|            | Lesson 1 (Mon)          | Lesson 2 (Tues)         | Lesson 3 (Wed)      | Lesson 4 (Thurs)    |
|------------|-------------------------|-------------------------|---------------------|---------------------|
| __T2 W12__ | 1. Intro to Waves       | 2. Hand back Test       | 3. Intro Continued  | 4. Intro Continued  |
| __T3 W1__  | 5. Doppler Effect       | 6. Doppler Effect       | 7. Doppler Effect   | 8. Doppler Effect   |
| __T3 W2__  | 9. Doppler Effect       | 10. Doppler Effect      | 11. Beating         | 12. Beating         |
| __T3 W3__  | _Exam Revision_         | _Exam Revision_         | _Exam Revision_     | _Exam Revision_     |
| __T3 W4__  | __School Exams__        | __School Exams__        | __School Exams__    | __School Exams__    |
| __T3 W5__  | __School Exams__        | __School Exams__        | _Return Exam_       | 13. Review          |
| __T3 W6__  | 14. Standing Waves      | 15. Standing Waves      | 16. Standing Waves  | 17. Standing Waves  |
| __T3 W7__  | 18. Music               | 19. Music               | 20. Interference    | 21. Interference    |
| __T3 W8__  | 22. Diffraction Grating | 23. Diffraction Grating | _Revision/Catch-Up_ | _Revision/Catch-Up_ |
| __T3 W9__  | _Revision/Catch-Up_     | _Revision_              | _Revision_          | __TEST__            |

### Lesson Plans

1. Intro to Waves
    - Transverse waves & longitudinal waves
    - Labelling a wave (peak, trough, wavelength, period)
    - Frequency vs period
    - Wave equation $v=f\lambda$
    - Mechanical vs EM waves
    - Quizziz: https://quizizz.com/admin/quiz/start_new/56fc7c19905a99040f5f2ef7
2. Hand back Test
    - Hand back previous unit (Electromag) test & revision
    - Starter: Quizlet flashcards & Quizlet Live: https://quizlet.com/83814791/waves-flash-cards/
    - Waves Revision Questions #1 Q1
3. Intro Continued
    - Notes with example calculations on using $T = \frac{1}{f}$ and $v=f\lambda$.
    - Textbook Activity 5A Q1-5
    - Waves Revision Questions #1 Q2
    - Physics Worksheet 3.3 Waves Q1-5
4. Intro Continued
    - Hand out test marking schedule & assign as homework over the holidays
    - Any material not finished in 3.
5. Intro to the Doppler Effect
    - Notes describing the doppler effect & its cause
    - Notes explaining the equations that may be used
    - Introduce the $f'=f\frac{v_{w}}{v_{w} \pm v_{s}}$