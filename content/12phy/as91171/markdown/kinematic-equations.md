---
title: Kinematic Equations
subtitle: 12PHYS - Mechanics
author: Finn LeSueur
date: 2020
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

## Starter

Try and solve this problem:

A car initially travelling at $13ms^{-1}$ rolls down a straight slope, accelerating at $0.6 ms^{-2}$ for $10 s$. How far does the car travel in this time?

---

## Kinematic Equations

Five variables - five equations!

\begin{align*}
    & v_{f} = v_{i} + at \\\\
    & d = \frac{v_{i} + v_{f}}{2}t \\\\
    & v_{f}^{2} = v_{i}^{2} + 2ad \\\\
    & d = v_{i}t + \frac{1}{2}at^{2} \\\\
    & d = v_{f}t - \frac{1}{2}at^{2}
\end{align*}

---

### How to Solve Equations

1. Knowns
2. Unknown
3. Formula
4. Substitute
5. Solve!

---

Try again:

A car initially travelling at $13ms^{-1}$ rolls down a straight slope, accelerating at $0.6 ms^{-2}$ for $10 s$. How far does the car travel in this time?

---

__Step One – “knowns”__

A car initially travelling at $13ms^{-1}$ rolls down a straight slope, accelerating at $0.6 ms^{-2}$ for $10 s$. How far does the car travel in this time?

$v_{i} = 13ms^{-1}, a=0.6ms^{-2}, t=10s$

---

__Step Two – "unknowns"__

A car initially travelling at $13ms^{-1}$ rolls down a straight slope, accelerating at $0.6 ms^{-2}$ for $10 s$. How far does the car travel in this time?

$d = ?, v_{f} = \text{ not needed}$

---

__Step Three – "formula"__

Which formula does not include $v_{f}$?

---

__Step Four - "subtitute"__

\begin{align*}
    & d = v_{f}t + \frac{1}{2}at^{2} \\\\
    & d = (13 \times 10) + (\frac{1}{2} \times 0.6 \times 10^{2}) \\\\
\end{align*}

---

__Step Five - "solve"__

\begin{align*}
    & d = v_{f}t + \frac{1}{2}at^{2} \\\\
    & d = (13 \times 10) + (\frac{1}{2} \times 0.6 \times 10^{2}) \\\\
    & d = 130 + 30 = 160m
\end{align*}

---

Here is another question…

A windsurfer initially travelling at $3 ms^{-1}$ is accelerated by a strong wind gust, at $0.08 ms^{-2}$. What would be the windsurfer’s speed when he has travelled $100 m$ since the wind gust started?

KUFSS – SHOW ALL WORKING

---

Try this now:

What time does it take for an airplane to decelerate uniformly from $120 ms^{-1}$ to a stop if the distance covered along the runway is $1500 m$?

---

Worksheet #5 (Q1, 2 and 3 only)
