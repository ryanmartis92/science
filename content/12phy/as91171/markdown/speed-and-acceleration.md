---
title: Speed & Acceleration
subtitle: 12PHYS - Mechanics
author: Finn LeSueur
date: 2020
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

## Who is the fastest?

- Andy can run 100m in 11.9 seconds
- Bob can run 100m in 10.8 seconds
- Chris can run 100m in 12.4 seconds

---

## Who is the fastest?

- Aaron can run 534m in 1 minute
- Billy can run 510m in 1 minute
- Cameron can run 452m in 1 minute

---

## Who is the fastest?

- Ash can run 0.3km in 45 seconds
- Bailey can run 420m in 1 minute
- Caleb can run 510m in 1.5 minutes

---

## Average Speed

\begin{align*}
    & v = \frac{d}{t} \\\\
    & d = \text{total distance travelled} \\\\
    & t = \text{time} \\\\
    & v = \text{speed}
\end{align*}

<aside class="notes">
    Ask class for units for each variable in the equation.
    Make note that these are Standard International units and should be used at all times.
</aside>

---

### What does $ms^{-1}$ mean?

- It stands for __meters per second__
- E.g. the speed of sound is $330ms^{-1}$
- _Sound travels $330m$ in one second_

---

### Example

Ash runs 315m in 45s. Calculate his average speed in __meters per second__.

---

Ash runs 315m in 45s. Calculate his average speed in __meters per second__.

\begin{align*}
    & d = 315m, t = 45s \\\\
    & v = \frac{d}{t} \\\\
    & v = \frac{315}{45} \\\\
    & v = 7ms^{-1}
\end{align*}

---

### The Speed Of

- A skydiver (freefall) = $53ms^{-1}$
- A handgun bullet = $660ms^{-1}$
- A car on the road = $50km/hr$
- A flying airplane = $1100kmh^{-1}$
- Light = $300,000,000$

---

### Question

A car is moving at a speed of $10ms^{-1}$. How far does the car travel in $12s$?

---

\begin{align*}
    & d = v \times t \\\\
    & d = 10 \times 12 \\\\
    & d = 120m
\end{align*}

---

### Question

A man is running at a speed of $4ms^{-1}$. How long does he take to run $100m$?

---

\begin{align*}
    & t = \frac{d}{v} \\\\
    & t = \frac{100}{4} \\\\
    & t = 25s
\end{align*}

---

### Average vs Instantaneous Velocity

Velocity may refer to __average velocity__ or __instantaneous velocity__.

The formula $v = \frac{d}{t}$ can only be used to calculate __average velocity__ or when __the velocity is constant__.

---

## Acceleration

_The rate of change in speed_

\begin{align*}
    & a = \frac{\Delta v}{t} \\\\
    & \Delta v = \text{ change in speed} \\\\
    & t = \text{ time} \\\\
    & a = \text{ acceleration}
\end{align*}

<aside class="notes">
    Ask class for units for each variable in the equation.
    Make note that these are Standard International units and should be used at all times.
</aside>

---

### What does $ms^{-2}$ mean?

_meters per second squared OR meters per second per second_

For example, $a=12ms^{-2}$ means that the velocity is increased by $12ms^{-1}$ every second.

---

### $\Delta$ = Delta

This is the difference between the __initial__ and the __final__ value.

\begin{align*}
    & \Delta = final - initial \\\\
    & \text{e.g. }\Delta v = v_{f} - v_{i}
\end{align*}

---

### Example

A man initially walking at $2.0ms^{-1}$ notices that his house is on fire so he speeds up to $11ms^{-1}$ in $1.3s$.

1. Calculate the change in speed
2. Calculate the acceleration

---

\begin{align*}
    & \Delta v = v_{f} - v_{i} \\\\
    & \Delta v = 11 - 2 = 9ms^{-1} \\\\
    & \\\\
    & a = \frac{\Delta v }{t} \\\\
    & a = \frac{9}{1.3} = 6.9ms^{-2}
\end{align*}

---

### Example

A cyclist who has been travelling at a steady speed of $4ms^{-1}$ starts to accelerate. If he accelerates at $2.5ms^{-2}$, how long will he take to reach a speed of $24ms^{-1}$?

---

\begin{align*}
    & a = \frac{\Delta v}{t} \\\\
    & t = \frac{\Delta v}{a} \\\\
    & t = \frac{24 - 4}{2.5} \\\\
    & t = 8s
\end{align*}

---

### More Examples

1. A car initially moving at $12.7ms^{-1}$ accelerates at $1.3ms^{-2}$ for __one minute__. What is the car's final speed?
2. A car decelerates at $1.8ms^{-2}$ for $9.4s$ to stop. What was the car's initial speed?

---

__1.__

\begin{align*}
    & a = \frac{v_{f} - v_{i}}{t} \\\\
    & a \times t = v_{f} - v_{i} \\\\
    & v_{f} = (a \times t) + v_{i} \\\\
    & v_{f} = (1.3 \times 60) + 12.7 = 90.7ms^{-1}
\end{align*}

---

__2.__

\begin{align*}
    & a = \frac{v_{f} - v_{i}}{t} \\\\
    & a \times t = v_{f} - v_{i} \\\\
    & v_{i} = v_{f} - (a \times t) \\\\
    & v_{i} = 0 - (-1.8 \times 9.4) = -16.92ms^{-1}
\end{align*}
