---
title: Practice Assessment
subtitle: 11SCI - Chemical Investigation
author: Finn LeSueur
date: 2020
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

## Practice Assessment

![](../assets/3-groups.png)

---

## Practice Assessment

- Pens at your desk
- Cheat sheet with you
- Working in groups today!

---

Group Work:

- Fill in the first section of the booklet
- Aim (from the instruction sheet)
- Hypothesis, independent, dependent and control variables
- Method & results table
- Conduct the experiment (day 2)

---

Individual Work:

- Record data
- Write up conclusion and evaluation
