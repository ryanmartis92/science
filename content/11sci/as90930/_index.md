---
title: AS90930 - Chemical Investigation
menu:
    main:
        parent: "11SCI"
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90930](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90930)

## Learning Outcomes

{{< slides >}}

## Unit Plan

|           | Lesson 1 (Mon)                    | Lesson 2 (Tues)       | Lesson 3 (Thurs)          | Lesson 4 (Fri)            |
|-----------|-----------------------------------|-----------------------|---------------------------|---------------------------|
| __T1 W7__ | __Genetics Test__                 | Rates of Reaction     | Rates of Reaction         | Collision Theory          |
| __T1 W8__ | Effect of Temperature             | Effect of Temperature | Effect of Concentration   | Effect of Concentration   |
| __T2 W7__ |                                   |                       | Planning an Investigation | Passing the Investigation |
| __T2 W8__ | __Queen's B-Day__                 | Practice Test         | Practice Test             | Feedback                  |
| __T2 W9__ | Changing Concentration & Revision | __Group Practical__   | __Individual Write-Up__   | __Individual Write-Up__   |
