---
title: Microbes and Food
menu:
    main:
        parent: "11SCI"
---

{{< slides >}}

## Term 2, Week 2

__Google Meets:__ Thursday 8:45am

### Lesson 1

- The Structure of Fungi & Fungi Reproduction Slides
- Education Perfect Task
- Google Forms Quiz

### Lesson 2

- Google Meet for questions & chat about fungi & viruses
- Check up on how we're all going

### Lesson 3

- Viruses Slides
- Education Perfect Task

### Lesson 4

- Extracellular Digestion in Bacteria and Fungi
- Google Forms Quiz

---

## Term 2, Week 3

### Lesson 1

- Make notes using the Yeast Fermentation slides

### Lesson 2

- If possible, do the Yeast Fermentation Experiment, you can substitute balloons for plastic bags or something else capable of containing the air.

### Lesson 3

- Catch up on any missed content & make sure you've handed everything in on Google Classroom.

---

## Term 2, Week 4

This week we are finishing learning the content we need to move on to our assessment for this unit. The assessment will take three weeks.

### Lesson 1

- Read through and make notes from the preservation slides
- Complete the Education Perfect task on factors affecting fungi and bacteria

### Lesson 2

- Read through and make notes from the making yoghurt slides
- Complete the Education Perfect task

---

## Assessment Task 1

The assessment for this unit is to produce a report investigating how we can use bacteria and fungi to help produce foodstuffs and link it back to the life processes of the microorganism.

To do this we need to first complete some research into this! We will spend two weeks doing this research.

- Task 1 Part 1: Research Beer Production, due Sunday May 10th @ 5pm
- Task 1 Part 2: Research Yoghurt Production, due Sunday May 17th @ 5pm
- Task 2: Produce an essay-format report on your research, due Sunday May 24th @ 5pm

---

## Term 2, Week 6

1. Recap research & prep for task 2
2. Assessment Task 2
    - Issue Task 2 via Google Classroom
    - Go through assessment document and ensure everyone understands what is expected and when it is due
    - Finish research & work on assessment task
3. Work on Task 2
4. Work on Task 2
