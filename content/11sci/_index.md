---
title: 11 Science
menu: "main"
linkTitle: "11SCI"
---

1. [AS90948 - Genetics](as90948/)
    - External, 4 Level 1 Credits 
2. [AS90950 - Microbes & Food](as90950/)
    - Internal, 4 Level 1 Credits
3. [AS90930 - Chemical Investigation](as90930/)
    - Internal, 4 Level 1 Credits
4. [AS90947 - Chemical Reactions](as90947/)
    - Internal, 4 Level 1 Credits
5. [AS90940 - Mechanics](as90940/)
    - External, 4 Level 1 Credits

## 2020 Year Planner

|         | Term 1         |                         | Term 2         |                         | Term 3     |                    | Term 4      |                |
|---------|----------------|-------------------------|----------------|-------------------------|------------|--------------------|-------------|----------------|
| Week 1  | __Jan 27th__   | Genetics                | __Apr 13th__   | Microbes                | __Jul 20__ | Chemical Reactions | __Oct 12__  | Mechanics      |
| Week 2  | __Feb 3rd__    | Genetics                | __April 20th__ | Microbes                | __Jul 27__ | Chemical Reactions | __Oct 19__  | Mechanics      |
| Week 3  | __Feb 10th__   | Genetics                | __April 27th__ | Microbes                | __Aug 3__  | Chemical Reactions | __Oct 26__  | Revision       |
| Week 4  | __Feb 17th__   | Genetics                | __May 4th__    | Microbes                | __Aug 10__ | __Senior Exams__   | __Nov 2__   | Prize Giving   |
| Week 5  | __Feb 24th__   | Genetics                | __May 11th__   | Microbes                | __Aug 17__ | __Senior Exams__   | __Nov 9__   | __NCEA Exams__ |
| Week 6  | __March 2nd__  | Genetics                | __May 18th__   | Microbes                | __Aug 24__ | Mechanics          | __Nov 16__  | __NCEA Exams__ |
| Week 7  | __March 9th__  | Chemistry Investigation | __May 25th__   | Chemistry Investigation | __Aug 31__ | Mechanics          | __Nov 23__  | __NCEA Exams__ |
| Week 8  | __March 16th__ | Chemistry Investigation | __June 1st__   | Chemistry Investigation | __Sep 7__  | Mechanics          | __Nov 30__  | __NCEA Exams__ |
| Week 9  | __March 23rd__ | Chemistry Investigation | __June 8th__   | Chemistry Investigation | __Sep 14__ | Mechanics          | __Dec 7th__ | __NCEA Exams__ |
| Week 10 |                |                         | __June 15th__  | Chemical Reactions      | __Sep 21__ | Mechanics          |             |                |
| Week 11 |                |                         | __June 22nd__  | Chemical Reactions      |            |                    |             |                |
| Week 12 |                |                         | __June 29th__  | Chemical Reactions      |            |                    |             |                |
