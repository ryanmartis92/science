---
title: Reactions Matter
menu:
    main:
        parent: "9SCIE"
---

{{< slides >}}

## Term 2, Week 2

__Google Meets:__ Wednesday 8:45am (9KNS), Wednesday 1:40pm (9FCG)

__Resources:__ Revision Notes

### Lesson 1: Revision

- Changes of State Worksheet
- Elements Compounds and Mixtures Worksheet

### Lesson 2

- Google Meet for questions & help!

### Lesson 3

- Solutions Worksheet
- Physical vs Chemical Changes Worksheet

Please, as always, hand in a photo of your working (if in your exercise book), or the document that you answered your questions in.

Comment below or email me LSF@cashmere.school.nz if you get stuck!

---

## Term 2, Week 3

This we are continuing our revision of Reactions Matter as we will are having our end of topic test next Tuesday evening. I will email with more details regarding the test later today!

Due to the long weekend, this will only be a two-lesson week, but I will try assign plenty of content.

### Lesson 1

- Complete the true/false worksheet. This should take you ~20-30 minutes if you take time to think about your answers and answer in-depth.
- Complete the freezing worksheet. This should take you 10-15 minutes if you take the time to do it thoroughly.

### Lesson 2

- Education Perfect Revision Task

Please comment on this weeks assignment or email me if you have any questions about any of the content! It is important that you clarify any confusions before the test.