---
title: Plants as Producers
menu:
    main:
        parent: "9SCIE"
---

{{< slides >}}

## Learning Outcomes

1. __Living Organisms__
    1. Identify and describe the difference between living and non-living
    2. Identify features of living organisms (MRS C GREN)
    3. Identify the major differences between plants and animals
2. __Plant Growth & Reproduction__
    1. Explain why plants are so important to life on Earth
    2. Describe and explain the process of photosynthesis
    3. Describe plant growth, reproduction, dispersal and why bees are so important.
3. __Plant vs. Animal Cells__
    1. Identify the main features of plant and animal cells
    2. Describe their functions
    3. Use a microscope to view cells of plants and animals

## Unit Plan

|            | L1 (Mon/Tues)         | L2 (Wed)                | L3 (Fri)                     |
|:-----------|:----------------------|:------------------------|:-----------------------------|
| __T2 W11__ | 1. Video/Finish E4L   | 2. Plant & Animal Cells | 3. Experiments & Microscopes |
| __T2 W12__ | 4. __P:__ Microscopes | 5. Graph Reading        | 6. Plant Reproduction        |
| __T3 W1__  | 7. Plant Reproduction | 8. Adaptations of Seeds | 9. __Catch-Up__              |
| __T3 W2__  | 10. Photosynthesis    | 11. Photosynthesis      | 12. Evergreen vs. Deciduous  |
| __T3 W3__  | 13. NOS               | 14. Revision            | 15. __TEST__                 |

### Lesson Plans

1. Video/Finish Energy for Life Test
    - Glue in Learning Outcomes
    - Watch _Life, Season 1, Episode 8: Plants (58min)_
2. Plant & Animal Cells
    - Watch video on an introduction to cells
    - Read the history of cell theory & answer associated questions
    - Move around the room using the stations to fill in notes about plant vs animal cell structures
    - Finish with a Quizziz: https://quizizz.com/admin/quiz/5a5f4eb1326b42000ff15aa4/cells
3. Aim, Hypothesis, Fair Testing & Microscope Use
    - Finish off labelling cell diagrams
    - Quizziz: https://quizizz.com/admin/quiz/5a5f4eb1326b42000ff15aa4/cells
    - Video on microscopes
    - Use EP task to help label microscope diagram in exercise books
    - Make notes on preparing a slide using experiment document
    - Read about and answer questions on the formation of cell theory
4. __Practical__: Microscopes
    - FCG do this on Monday P4, KNS to do this Friday P5.
5. Graph Reading
6. Plant Reproduction
7. Plant Reproduction
    - Cut and paste reproduction cycle of plants
    - Flashcards on the function of the anther, filament, stigma, style, ovary and ovule.
    - Spelling practice: https://www.aaaspell.com/spelling_lists/4118311
8. Adaptations of Seeds
    - The overall function of seeds
    - What is it important that seeds do
    - Possible adaptations to achieve this
    - Spelling practice: https://www.aaaspell.com/spelling_lists/4118311
9. __Catch-Up__
    - Flower dissection practical: https://www.riskassess.co.nz/risk_assessment/8324197
    - Spelling practice: https://www.aaaspell.com/spelling_lists/4118311
10. Photosynthesis
    - Equation
    - Description of why it is important
    - Relate its efficiency to the seasons
    - Spelling practice: https://www.aaaspell.com/spelling_lists/4118311
11. Photosynthesis
    - Spelling practice: https://www.aaaspell.com/spelling_lists/4118311
12. Evergreen vs. Deciduous
    - Relating photosynthesis to graphs to determine evergreen/deciduous
    - __Spelling Test__
13. Nature of Science - Plant Importance
14. Revision
    - Fair tests
15. __TEST__
