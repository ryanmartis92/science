---
title: Plant Reproduction
subtitle: 9SCIE - Plants as Producers
author: Finn LeSueur
date: 2020
theme: finn
weight: 2
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

## Learning Outcomes

- Describe plant growth, reproduction, dispersal and why bees are so important.

---

## Starter

1. What are some organelles that only __plant__ cells have?
2. What are some organelles that both __plant__ and __animal__ cells have?

---

Plants, like animals, need to reproduce to survive. Without reproduction there would be no more plants.

But how do they do it?!

---

### Reproductive Cycle

1. Plants produce pollen
2. This pollen can be carried away from the plant by animals, wind or rain
3. Once on a different plan, this pollen can fertilise the flower by landing on its stigma
4. The pollen may _germinate_ (grow down into) the ovule of the plant
5. This can result in in the growth of seeds or fruit

---

The seeds from the fruit can then grow through mitosis (once in) the ground to form new plants!

---

### Self and Cross-Pollinating

- __Self-Pollinating__: The plant can fertilise itself
- __Cross-Pollinating__: The plant needs to get pollen from another flower of the same species

---

### Task

1. Glue in your flower diagram
2. Glue in your reproductive cycle diagram

---

### Task

1. Watch the video linked on Google Classroom.
2. Pause the video to answer the questions on the associated document __in your book__.
	- The questions follow the order of the video.
